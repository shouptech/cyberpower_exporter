*******************
Cyberpower Exporter
*******************

This is an application for exporting cyberpower ups metrics to Prometheus.

Installation
============

First, install the CyberPower PowerPanel Personal Linux software: https://www.cyberpowersystems.com/product/software/power-panel-personal/powerpanel-for-linux/

Second, install a virtual environment, and install cyberpower_exporter to the environment:

::
    sudo python3 -mvenv /opt/cyberpower_exporter
    sudo /opt/cyberpower_exporter/bin/pip install cyberpower-exporter --index-url https://gitlab.com/api/v4/projects/17596547/packages/pypi/simple

Installing as root is important. The application must run as root in order to access the cyberpower data.

You could simply run it as root like this:

::
    sudo /opt/cyberpower_exporter/bin/cyberpower_exporter

But that would not be great because it wouldn't run in the background. If you're using systemd, it's easy to make a service.

Create the file /etc/systemd/system/cyberpower_exporter.service with the contents from the file: `cyberpower_exporter.service`_

Then enable & start the service:

::

    $ sudo systemctl daemon-reload
    $ sudo systemctl enable cyberpower_exporter.service
    Created symlink /etc/systemd/system/multi-user.target.wants/cyberpower_exporter.service → /etc/systemd/system/cyberpower_exporter.service.
    $ sudo systemctl start cyberpower_exporter.service

If you have a firewall enabled, ensure you allow port 10100 through.

Test that it's working by running:

::

    curl <ipaddress>:10100

Now go ahead and point prometheus to it!


Changelog
=========

0.2.1
-----

* Updated build process to upload to GitLab package registry.
* Updated README to have instructions for installing from GitLab package registry.

0.2
----

* Created environment variable ``CYBERPOWER_EXPORTER_POLL_INTERVAL`` for adjusting
  polling interval.
* Moved input rating and output rating to prometheus gauges to make it easier to use
  those data points in dashboards.

0.1
----

Initial release
